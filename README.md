<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Proyecto de Gestor de Tareas</h1>
    <br>
</p>


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources




INSTALLATION
------------

### Install via Composer


Ejecuta esto:

~~~
composer create-project marinervi/gestor-tareas carpeta
~~~

En carpeta pon el nombre donde quieres que se cree tu proyecto


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=BBDD',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];


**NOTES:**
- BBDD es el  nombre de la base de datos que quieras utilizar con este proyecto
- Tiene una tabla de tareas y otra tabla de usuarios


