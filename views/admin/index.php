<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

   
    <div class="col-lg-10">
    <?= GridView::widget([
         'dataProvider' => $dataProvider,
            'columns' => [
            'id',
            'username',
            'email:email',
            'nombre',
            'apellidos',
            ['class' => 'yii\grid\ActionColumn'],
        ],
        ]) 
        ?>
    </div>

</div>